# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/qflipper/issues
# Bug-Submit: https://github.com/<user>/qflipper/issues/new
# Changelog: https://github.com/<user>/qflipper/blob/master/CHANGES
# Documentation: https://github.com/<user>/qflipper/wiki
# Repository-Browse: https://github.com/<user>/qflipper
# Repository: https://github.com/<user>/qflipper.git
